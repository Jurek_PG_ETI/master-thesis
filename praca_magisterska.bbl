\begin{thebibliography}{10}

\bibitem{tadeusiewicz}
R.~Tadeusiewicz, {\em Sygnał Mowy}.
\newblock Wydawnictwa Komunikacji i Łączności, 1988.

\bibitem{ziolko}
B.~Ziółko and M.~Ziółko, {\em Przetwarzanie mowy}.
\newblock Wydawnictwo AGH, 2011.

\bibitem{ciota}
Z.~Ciota, {\em Metody przetwarzania sygnałów akustycznych w komputerowej
  analizie mowy}.
\newblock Warszawa: Akademicka Oficyna Wydawnicza Exit, 2010.

\bibitem{jurafsky}
D.~Jurafsky and J.~H. Martin, {\em Speech and Language Processing. An
  Introduction to Natural Language Processing, Computational Linguistics and
  Speech Recognition}.
\newblock Prentice Hall, second~ed., 2008.

\bibitem{slot}
K.~Ślot, {\em Wybrane zagadnienia biometrii}.
\newblock Wydawnictwa Komunikacji i Łączności, 2008.

\bibitem{bolle}
R.~M. Bolle, J.~H. Connell, S.~Pankanti, N.~K. Ratha, and A.~W. Senior, {\em
  Biometria}.
\newblock Wydawnictwa Naukowo-Techniczne, 2008.

\bibitem{jin}
Q.~Jin and T.~F. Zheng, ``Overview of front-end features for robust speaker
  recognition,'' {\em APSIPA ASC}, 2011.

\bibitem{reynolds2000}
D.~A. Reynolds, T.~F. Quatieri, and R.~B. Dunn, ``Speaker verification using
  adapted gaussian mixture models,'' {\em Digital Signal Processing}, vol.~10,
  pp.~19--41, 2000.

\bibitem{reynolds1995}
D.~A. Reynolds, ``Speaker identification and verification using gaussian
  mixture speaker models,'' {\em Speech communication}, vol.~17, no.~1,
  pp.~91--108, 1995.

\bibitem{laskowski}
K.~Laskowski and Q.~Jin, ``Modeling instantaneous intonation for speaker
  identification using the fundamental frequency variation spectrum,'' in {\em
  Proceedings of the {IEEE} International Conference on Acoustics, Speech, and
  Signal Processing, {ICASSP} 2009, 19-24 April 2009, Taipei, Taiwan},
  pp.~4541--4544, 2009.

\bibitem{mansouri}
A.~Mansouri, J.~Cardenas-Barrera, and E.~Castillo-Guerra, ``A study on
  dimensions of feature space for text-independent speaker verification
  systems,'' in {\em 2015 IEEE 28th Canadian Conference on Electrical and
  Computer Engineering (CCECE)}, pp.~1464--1469, May 2015.

\bibitem{dehak}
N.~Dehak, P.~J. Kenny, R.~Dehak, P.~Dumouchel, and P.~Ouellet, ``Front-end
  factor analysis for speaker verification,'' {\em IEEE Transactions on Audio,
  Speech, and Language Processing}, vol.~19, pp.~788--798, May 2011.

\bibitem{kanrar}
S.~Kanrar and N.~Jaiswal, ``Text and language independent speaker
  identification by gmm based i vector,'' in {\em Proceedings of the Sixth
  International Conference on Computer and Communication Technology 2015},
  ICCCT '15, (New York, NY, USA), pp.~95--100, ACM, 2015.

\bibitem{chen}
K.~Chen and A.~Salman, ``Learning speaker-specific characteristics with a deep
  neural architecture,'' {\em IEEE Transactions on Neural Networks}, vol.~22,
  pp.~1744--1756, Nov 2011.

\bibitem{liu}
Y.~Liu, T.~Fu, Y.~Fan, Y.~Qian, and K.~Yu, ``Speaker verification with deep
  features,'' in {\em 2014 International Joint Conference on Neural Networks
  (IJCNN)}, pp.~747--753, July 2014.

\bibitem{li}
C.~Li, X.~Ma, B.~Jiang, X.~Li, X.~Zhang, X.~Liu, Y.~Cao, A.~Kannan, and Z.~Zhu,
  ``Deep speaker: an end-to-end neural speaker embedding system,'' {\em CoRR},
  vol.~abs/1705.02304, 2017.

\bibitem{reynolds1990}
R.~C. Rose and D.~A. Reynolds, ``Text independent speaker identification using
  automatic acoustic segmentation,'' in {\em International Conference on
  Acoustics, Speech, and Signal Processing}, pp.~293--296 vol.1, Apr 1990.

\bibitem{reynolds2003}
A.~G. Adami, R.~Mihaescu, D.~A. Reynolds, and J.~J. Godfrey, ``Modeling
  prosodic dynamics for speaker recognition,'' in {\em Acoustics, Speech, and
  Signal Processing, 2003. Proceedings. (ICASSP '03). 2003 IEEE International
  Conference on}, vol.~4, pp.~IV--788--91 vol.4, April 2003.

\bibitem{reynolds2011}
N.~Dehak, Z.~N. Karam, D.~A. Reynolds, R.~Dehak, W.~M. Campbell, and J.~R.
  Glass, ``A channel-blind system for speaker verification,'' in {\em 2011 IEEE
  International Conference on Acoustics, Speech and Signal Processing
  (ICASSP)}, pp.~4536--4539, May 2011.

\bibitem{davis}
S.~Davis and P.~Mermelstein, ``Comparison of parametric representations for
  monosyllabic word recognition in continuously spoken sentences,'' {\em IEEE
  Transactions on Acoustics, Speech, and Signal Processing}, vol.~28,
  pp.~357--366, Aug 1980.

\bibitem{soong}
F.~K. Soong and A.~E. Rosenberg, ``On the use of instantaneous and transitional
  spectral information in speaker recognition,'' {\em IEEE Transactions on
  Acoustics, Speech, and Signal Processing}, vol.~36, pp.~871--879, Jun 1988.

\bibitem{bishop}
C.~M. Bishop, {\em Pattern Recognition and Machine Learning (Information
  Science and Statistics)}.
\newblock Secaucus, NJ, USA: Springer-Verlag New York, Inc., 2006.

\bibitem{pollard}
J.~E.~V. Pollard H.~F., ``A tristimulus method for the specification of musical
  timbre,'' {\em Acta Acustica united with Acustica}, vol.~51, pp.~162--171,
  August 1982.

\bibitem{asa}
Acoustical Society of America, {\em Acoustical Terminology ANSI S1.1-1994 (ASA
  111-1994)}.

\bibitem{peteers}
G.~Peeters, ``A large set of audio features for sound description (similarity
  and classification) in the cuidado project,'' 2004.

\bibitem{zwicker}
E.~Zwicker, ``Subdivision of the audible frequency range into critical bands
  (frequenzgruppen),'' {\em {Journal of the Acoustical Society of America}},
  vol.~33, pp.~248--249, 1961.

\bibitem{hinton_dropout}
G.~Hinton, N.~Srivastava, A.~Krizhevsky, I.~Sutskever, and R.~Salakhutdinov,
  ``Improving neural networks by preventing co-adaptation of feature
  detectors,'' {\em arXiv preprint}, 2012.

\bibitem{karpathy}
A.~Karpathy, ``Cs231n: Convolutional neural networks for visual recognition,''
  2016.

\bibitem{malina}
W.~Malina and M.~Smiatacz, {\em Metody Cyfrowego Przetwarzania Obrazów}.
\newblock Warszawa: EXIT, 2005.

\bibitem{yosinski}
J.~Yosinski, J.~Clune, A.~Nguyen, T.~Fuchs, and H.~Lipson, ``Understanding
  neural networks through deep visualization,'' in {\em Deep Learning Workshop,
  International Conference on Machine Learning (ICML)}, 2015.

\bibitem{hinton}
A.~Krizhevsky, I.~Sutskever, and G.~E. Hinton, ``Imagenet classification with
  deep convolutional neural networks,'' {\em Advances in Neural Information
  Processing Systems}, vol.~25, 2012.

\bibitem{he}
K.~He, X.~Zhang, S.~Ren, and J.~Sun, ``Delving deep into rectifiers: Surpassing
  human-level performance on imagenet classification,'' {\em CoRR},
  vol.~abs/1502.01852, 2015.

\bibitem{szegedy}
S.~Ioffe and C.~Szegedy, ``Batch normalization: Accelerating deep network
  training by reducing internal covariate shift,'' {\em CoRR},
  vol.~abs/1502.03167, 2015.

\bibitem{goodfellow}
I.~Goodfellow, Y.~Bengio, and A.~Courville, {\em Deep Learning}.
\newblock MIT Press, 2016.

\bibitem{lukic}
Y.~Lukic, C.~Vogt, O.~Durr, and T.~Stadelmann, ``Speaker identification and
  clustering using convolutional neural networks,'' {\em 2016 IEEE 26th
  International Workshop on Machine Learning for Signal Processing (MLSP)},
  2016.

\bibitem{xavier}
X.~Glorot and Y.~Bengio, ``Understanding the difficulty of training deep
  feedforward neural networks,'' in {\em In Proceedings of the International
  Conference on Artificial Intelligence and Statistics (AISTATS’10). Society
  for Artificial Intelligence and Statistics}, 2010.

\bibitem{guido}
http://legacy.python.org/doc/essays/blurb/.
\newblock Data dostępu: 24.09.2017.

\bibitem{pythonr}
http://www.kdnuggets.com/2017/08/python-overtakes-r-leader-analytics-data-science.html.
\newblock Data dostępu: 24.09.2017.

\bibitem{numpy}
http://www.numpy.org.
\newblock Data dostępu: 23.09.2017.

\bibitem{matplotlib}
http://www.matplotlib.org.
\newblock Data dostępu: 23.09.2017.

\bibitem{sklearn}
http://www.scikit-learn.org.
\newblock Data dostępu: 23.09.2017.

\bibitem{tensorflow}
http://www.tensorflow.org.
\newblock Data dostępu: 23.09.2017.

\bibitem{soundfile}
http://github.com/bastibe/PySoundFile.
\newblock Data dostępu: 23.09.2017.

\bibitem{psf}
https://goo.gl/uNYfGC.
\newblock Data dostępu: 23.09.2017.

\bibitem{librosa}
http://github.com/librosa/librosa.
\newblock Data dostępu: 23.09.2017.

\end{thebibliography}
