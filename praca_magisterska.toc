\contentsline {section}{Wykaz ważniejszych skrótów i oznaczeń}{7}
\contentsline {section}{\numberline {1}Wstęp}{8}
\contentsline {subsection}{\numberline {1.1}Analiza sygnału mowy}{8}
\contentsline {subsection}{\numberline {1.2}Biometria głosowa}{8}
\contentsline {subsubsection}{\numberline {1.2.1}Rodzaje biometrii głosowej}{9}
\contentsline {subsubsection}{\numberline {1.2.2}Miary jakości}{10}
\contentsline {subsubsection}{\numberline {1.2.3}Możliwe zastosowania i zagrożenia}{10}
\contentsline {subsection}{\numberline {1.3}Cel pracy i treść rozdziałów}{12}
\contentsline {section}{\numberline {2}Przegląd istniejących rozwiązań}{13}
\contentsline {section}{\numberline {3}Rozwiązanie bazowe}{15}
\contentsline {subsection}{\numberline {3.1}Cechy mel-cepstralne}{15}
\contentsline {subsection}{\numberline {3.2}Mieszaniny gaussowskie}{18}
\contentsline {subsection}{\numberline {3.3}Uniwersalny model mówcy}{19}
\contentsline {section}{\numberline {4}Propozycja rozwiązania}{22}
\contentsline {subsection}{\numberline {4.1}Wektor cech spektralnych}{22}
\contentsline {paragraph}{•}{23}
\contentsline {paragraph}{•}{23}
\contentsline {paragraph}{•}{23}
\contentsline {paragraph}{•}{23}
\contentsline {paragraph}{•}{24}
\contentsline {paragraph}{•}{24}
\contentsline {paragraph}{•}{25}
\contentsline {paragraph}{•}{26}
\contentsline {subsection}{\numberline {4.2}Neuronowy ekstraktor cech}{27}
\contentsline {subsubsection}{\numberline {4.2.1}Splotowe Sieci Neuronowe}{27}
\contentsline {subsubsection}{\numberline {4.2.2}Sieć do identyfikacji mówców}{31}
\contentsline {subsubsection}{\numberline {4.2.3}Ekstrakcja cech}{33}
\contentsline {section}{\numberline {5}Implementacja aplikacji do weryfikacji mówcy}{36}
\contentsline {subsection}{\numberline {5.1}Python - wiodący język w uczeniu maszynowym}{36}
\contentsline {subsection}{\numberline {5.2}Wykorzystane biblioteki}{36}
\contentsline {paragraph}{•}{36}
\contentsline {paragraph}{•}{37}
\contentsline {paragraph}{•}{37}
\contentsline {paragraph}{•}{37}
\contentsline {paragraph}{•}{37}
\contentsline {paragraph}{•}{37}
\contentsline {paragraph}{•}{37}
\contentsline {subsection}{\numberline {5.3}Opis implementacji rozwiązania}{38}
\contentsline {section}{\numberline {6}Wyniki}{40}
\contentsline {subsection}{\numberline {6.1}Wykorzystane bazy nagrań}{40}
\contentsline {subsection}{\numberline {6.2}Metodologia testów}{40}
\contentsline {subsection}{\numberline {6.3}Osiągnięte rezultaty}{41}
\contentsline {subsubsection}{\numberline {6.3.1}Baza 1}{41}
\contentsline {subsubsection}{\numberline {6.3.2}Baza 2}{42}
\contentsline {section}{\numberline {7}Podsumowanie}{47}
\contentsline {subsection}{\numberline {7.1}Wnioski}{47}
\contentsline {subsection}{\numberline {7.2}Możliwości dalszego rozwoju}{47}
\contentsline {section}{Wykaz literatury}{51}
\contentsline {section}{Wykaz rysunków}{53}
\contentsline {section}{Wykaz tabel}{54}
\contentsline {section}{Dodatek A}{55}
\contentsline {section}{Dodatek B}{55}
